/**
 * @file key.c
 * @brief  key驱动程序（基于STM32的v3.5库）
 * @author 王晓荣
 * @version 
 * @date 2014-03-19
 */

#include "key.h"
#include "delay.h"


const struct KEY_PIN key_pin[] = 
{
    {{GPIOA,  GPIO_Pin_0}, KEY_1},
    {{GPIOC, GPIO_Pin_13}, KEY_2},
};

#define NUM_KEYS (sizeof(key_pin)/sizeof(struct KEY_PIN))


/**
 * @brief 按键配置
 */
void key_config (void) 
{
    uint8_t          n;
    GPIO_InitTypeDef GPIO_InitStructure;

    for (n = 0; n < NUM_KEYS; n++) 
    {      
        gpio_clock_enable(key_pin[n].gpio_pin.port);      
        GPIO_InitStructure.GPIO_Pin   = key_pin[n].gpio_pin.pin;  
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
        GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IPU;
        GPIO_Init(key_pin[n].gpio_pin.port, &GPIO_InitStructure);    
    }    
}

/**
 * @brief 得到按键号
 *
 * @return 按键号 
 */
enum KEY_N key_get(void) 
{
    uint8_t n;
    uint8_t key_down_flg = 0;
    uint8_t key_up_num = 0;
    enum KEY_N key = KEY_NULL;  

    for(n = 0; n < NUM_KEYS; n++)
    {       
        if(GPIO_ReadInputDataBit(key_pin[n].gpio_pin.port, key_pin[n].gpio_pin.pin) == 0)
        {
            key_down_flg = 1;
            break;
        }
    }
    if(key_down_flg)
    {
        delay(50000);
        for(n = 0; n < NUM_KEYS; n++)
        {
            if(GPIO_ReadInputDataBit(key_pin[n].gpio_pin.port, key_pin[n].gpio_pin.pin) == 0)
            {
                key = key_pin[n].numb;
                break;
            }
            if(n >= NUM_KEYS - 1)
            {
                key = KEY_NULL;
            }
        }
        while(1)
        {
            delay(1000);
            key_up_num = 0;
            for(n = 0; n < NUM_KEYS; n++)
            {
                if(GPIO_ReadInputDataBit(key_pin[n].gpio_pin.port, key_pin[n].gpio_pin.pin))
                {
                    key_up_num++;
                }
            }
            if(key_up_num >= NUM_KEYS) 
            {
                break;
            }
        }
        return(key);					
	}	
	return(KEY_NULL);
}

/**
 * @brief 得到按键数量 
 *
 * @return 按键数量 
 */
uint8_t key_num(void)
{
    return(NUM_KEYS);
}

