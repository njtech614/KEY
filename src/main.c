/**
 * @file main.c
 * @brief 按键控制LED显示0-15二进制数（基于STM32的v3.5库）
 * @author 王晓荣
 * @version 
 * @date 2014-03-19
 */

#include "led.h" 
#include "key.h"


/**
 * @brief 主程序
 *
 * @return 
 */
int main(void)
{
    enum KEY_N key  = KEY_NULL; 
    uint8_t    temp = 0;
    
    led_config();   
    key_config(); 	
    
    while(1)
    {
        key = key_get();
        if(key == KEY_1)
        {
            if(temp >= 15)
            {
                temp = 0;
            }
            else
            {
                temp++;
            }
        }
        if(key == KEY_2)
        {
            if(temp == 0)
            {
                temp = 15;
            }
            else
            {
                temp--;
            }
        }
        leds_switch(temp);	
    }
}



