/**
 * @file led.c
 * @brief  led驱动程序（基于STM32的v3.5库）
 * @author 王晓荣
 * @version 
 * @date 2014-03-19
 */

#include "led.h"


const struct GPIO_PIN led_pin[] =
{
    { GPIOD,   GPIO_Pin_2},
    { GPIOD,   GPIO_Pin_3},
    { GPIOD,   GPIO_Pin_4},
    { GPIOD,   GPIO_Pin_7},
};

#define NUM_LEDS (sizeof(led_pin)/sizeof(struct GPIO_PIN))

/**
 * @brief led配置
 */
void led_config(void) 
{
    uint8_t          n;
    GPIO_InitTypeDef GPIO_InitStructure;

    for (n = 0; n < NUM_LEDS; n++) 
    {      
        gpio_clock_enable(led_pin[n].port);      
        GPIO_InitStructure.GPIO_Pin   = led_pin[n].pin;  
        GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
        GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_Out_PP;
        GPIO_Init(led_pin[n].port, &GPIO_InitStructure);    
    }
}

/**
 * @brief  点亮某序号的LED 
 *
 * @param num LED的序号（从0开始）
 */
void led_on(uint8_t num) 
{
    //GPIO_WriteBit(led_pin[num].port, led_pin[num].pin, Bit_RESET);
    GPIO_WriteBit(led_pin[num].port, led_pin[num].pin, Bit_SET);
}

/**
 * @brief 关闭某序号的LED
 *
 * @param num
 */
void led_off(uint8_t num) 
{
    //GPIO_WriteBit(led_pin[num].port, led_pin[num].pin, Bit_SET);
    GPIO_WriteBit(led_pin[num].port, led_pin[num].pin, Bit_RESET);
}

/**
 * @brief 多个LED控制
 *
 * @param val 多个LED控制的二进制值
 */
void leds_switch(uint32_t val) 
{
    uint8_t n;
    for(n = 0; n < NUM_LEDS; n++)
    {
        if (val & (1 << n)) 
        {
            led_on(n);
        }
        else
        {
            led_off(n);
        }
    }
}

/**
 * @brief 得到LED数量
 *
 * @return LED数量 
 */
uint8_t led_get_num(void) 
{
    return(NUM_LEDS);
}

