/**
 * @file key.h
 * @brief  key驱动程序（基于STM32的v3.5库）
 * @author 王晓荣
 * @version 
 * @date 2014-03-19
 */

#ifndef __KEY_H
#define __KEY_H 

#include "gpio.h"

enum KEY_N
{
	KEY_1, KEY_2, KEY_NULL = 0xff,
}; 

struct KEY_PIN
{
    struct GPIO_PIN gpio_pin;
    enum   KEY_N    numb;
};

void       key_config (void); 
enum KEY_N key_get(void); 
uint8_t    key_get_num(void);

#endif
